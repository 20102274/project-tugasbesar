import './models/places.dart';
import './models/category.dart';

const CATEGORIES_DUMMY_DATA = const [
  Category(
    id: 'P1',
    title: 'Pantai',
    description: '',
    image: 'https://cdn.pixabay.com/photo/2020/09/16/18/39/icon-5577198_960_720.png',
  ),
  Category(
    id: 'P2',
    title: 'Gunung',
    description: '',
    image: 'https://cdn.icon-icons.com/icons2/567/PNG/512/mountain_icon-icons.com_54385.png',
  ),
  Category(
    id: 'P3',
    title: 'Budaya',
    description: '',
    image: 'https://media.istockphoto.com/id/887270584/id/vektor/budaya-indonesia-simbol-indonesia-yang-digambar-tangan.jpg?s=612x612&w=0&k=20&c=rvUpJkDKIO8vADXNxLwlq5pmyMKGQ7Obff0xkEfRXKk=',
  ),
  Category(
    id: 'P4',
    title: 'Kuliner',
    description: '',
    image: 'https://cdn-icons-png.flaticon.com/512/2934/2934069.png',
  ),
  Category(
    id: 'P5',
    title: 'Religi',
    description: '',
    image: 'https://cdn-icons-png.flaticon.com/512/5333/5333057.png',
  ),
  Category(
    id: 'P6',
    title: 'Edukasi',
    description: '',
    image: 'https://img.freepik.com/free-icon/mortarboard_318-205918.jpg?w=2000',
  ),
];

const PLACES_DUMMY_DATA = const [
  Places(
    id: 'A1',
    name: 'Pantai Menganti',
    description: 'Pantai ini dapat Anda temukan di Desa Karangduwur, Kecamatan Ayah, Kebumen. Jika Anda datang dari arah Purwokerto, ambil jalur menuju Pantai Ayah atau Pantai Logending. Saat menuju ke jalur tersebut, Anda akan menemukan persimpangan arah ke kanan dan ke kiri. Anda bisa mengambil persimpangan dengan arah ke kiri. Nantinya, Anda akan menemui jalan menanjak. Untuk sampai ke Pantai Menganti Anda membutuhkan waktu sekitar 20 hingga 30 menit lamanya. Meski punya bumbu-bumbu cerita mistis, namun keindahan Pantai Menganti memang tidak boleh disangkal. Dengan harga tiket masuk cuma Rp5.000 (belum ongkos parkir), Anda akan menemukan hamparan pasir pantai berwarna putih halus, berpadu serasi dengan air laut yang biru. Ketika cuaca sedang cerah, pantulan awan langit akan menambah keindahan pantai tersebut.',
    author: 'Gibran',
    category: 'P1',
    image: "https://travelspromo.com/wp-content/uploads/2022/08/Eksotisnya-Pantai-Menganti.-Foto-Gmap-Eko-Purwadi-1024x767.jpg",
    price: 5000
  ),    

  Places(
    id: 'A2',
    name: 'Pantai Suwuk',
    description: 'Seperti namanya, tempat ini berada di Dusun Suwuk, Desa Tambakmulyo, Kecamatan Puring, Kabupaten Kebumen. Jika Anda bertolak dari Banyumas, Anda bisa menempuh waktu sekitar 2 jam berkendara dengan melewati Jalan Ayah dan Karang Bolong. Pantai Suwuk menjadi muara dari Sungai Telomoyo yang mengalir di sepanjang wilayah Kebumen. Muara yang terletak di sebelah barat pantai ini juga menjadi pemisah antara Pantai Suwuk dengan Pantai Karang Bolong. Bila Anda berminat untuk menyusuri sungai, silakan siapkan kocek sebesar Rp25 ribu saja untuk menaiki perahu. Pemandangan perbukitan pun bisa Anda saksikan di pantai ini. Tak hanya satu, bukit tersebut berjumlah tiga, yang berada di sebelah barat pantai. Berbeda dengan bukit di sepanjang pantai Gunung Kidul yang berupa karang dan cenderung gersang, deretan bukit-bukit di Pantai Suwuk ini berupa kapur tetapi subur dan berwarna hijau yang mampu menyejukkan mata. Sisi menarik lain dari objek wisata ini adalah dengan adanya bangunan pesawat Boeing 737. Bangunan ini bukanlah merupakan bangkai pesawat asli, melainkan sengaja dibangun untuk memaksimalkan fungsi pantai sebagai destinasi wisata. Untuk bisa berkunjung ke tempat ini, wisatawan hanya dikenakan tiket masuk seharga Rp4.500 per orang.',
    author: 'Nanda ananda',
    category: 'P1',
    image: "https://cdn.idntimes.com/content-images/community/2022/01/15035694-530047617193721-7296532837514084352-n-9cde86371d7fc78c91ae80a6ffab250e-5bdeddb332a99fa043d0163eee95d3a4_600x400.jpg",
    price: 4500
  ),    

  Places(
    id: 'A3',
    name: 'Pantai Sodong',
    description: 'Rekomendasi pantai yang dekat dengan Purwokerto berikutnya, ada yang namanya Pantai Sodong. Wisata keluarga berikut termasuk yang menawarkan paket komplit wahana yang ramah untuk anak-anak.Karakter pantai satu ini memiliki pasir hitam, kontur bebatuan, dan disekelilingnya terdapat tebing perbukitan hijau. Kamu bisa menikmati fasilitas seperti warung makan atau jajanan, wisata ATV, kolam renang, spot-spot tempat duduk dibawah pepohonan, dan masih banyak lainnya.Tempatnya lumayan bersih, luas, sehingga kamu bisa eksplore sudut pantai sekaligus cari tempat buat spot foto yang estetik. Dan untuk harga tiket masukkan tergolong murah, cuma Rp6.500 saja.',
    author: 'Ivan Aveldo',
    category: 'P1',
    image: "https://mytrip123.com/wp-content/uploads/2021/12/pantai-sodong.jpg",
    price: 6500
  ),        

  Places(
    id:'A4', 
    name: 'GUNUNG SLAMET',
    category: 'P2', 
    description: 'Gunung Slamet adalah sebuah gunung berapi kerucut tipe A yang berada di Jawa Tengah, Indonesia. Gunung Slamet memiliki ketinggian 3.432 mdpl dan terletak di antara 5 kabupaten, yaitu Kabupaten Banyumas, Kabupaten Purbalingga, Kabupaten Brebes, Kabupaten Tegal dan Kabupaten Pemalang', 
    image: "https://upload.wikimedia.org/wikipedia/commons/2/2c/Mount_Slamet.jpg", 
    author: 'RIFKY MINA', 
    price: 15000
  ),

  Places(
    id: 'A5',
    name: 'Makam Mbah Atas Angin',
    description: 'Makam ini berlokasi di Baturaden yang dikenal dengan pancuran air panasnya, yakni Pancuran Pitu. Lokasinya berada di sebelah selatan Gunung Slamet atau menempuh kurang lebih menghabiskan waktu sekitar 40 menit dengan berkendara santai. Untuk tiket masuk, sudah menjadi satu kesatuan dengan wisata Pancuran Pitu sehingga pengunjung yang datang, selain untuk menikmati pemandian air panas juga bisa melakukan ritual ziarahnya. Makam Mbah Atas Angin ini adalah tempat dimakamkannya seorang penyebar agama Islam dari Turki bernama Syeh Maulana Magribi. Setelah melihat sebuah petunjuk berupa cahaya , beliau pun terus mengikutinya dan sampailah bersama pengikutnya yang bernama Haji Datuk di tanah Jawa ini. Syeh Maulana Magribi pun dijuluki  sebagai Mbah Atas Angin.',
    author: 'Syiam',
    category: 'P5',
    image: "https://awsimages.detik.net.id/api/wm/2021/02/04/dev-misteri-makam-keramat-mbah-atas-angin-di-baturaden_169.jpeg?wid=54&w=650&v=1&t=jpeg",
    price: 14000
  ),

  Places(
    id: 'A6',
    name: 'Masjid Saka Tunggal',
    description: 'Masjid Sakat Tunggal terletak sebelah barat daya Banyumas, bertempat di desa Cikakak Kecamatan Wangon. Menurut Juri Kunci Masjid tersebut, masjid ini merupakan masjid tertua di Nusantara. Meski belum terbukti kebenarannya namun masyarakat disana meyakini akan hal itu. Bangunan masjid Saka tunggal terbilang unik ini bisa dilihat dari namanya Masjid Saka Tunggal yang kalau diartikan dalam bahasa Banyumasan adalah Masjid Tiang Satu. Memang di dalam masjid ini hanya ada satu tiang besar yang menopa masjid ini. Tiang itu terletak ditengah-tengah Masjid ini. Dalam setiap arsitekturnya tedapat simbol-simbol yang bermakna kehidupan yang hanya bisa dijelaskan oleh juru kunci masjid tersebut. Salah-satu hal yang unik di Masjid ini adalah terdapat sekumpulan kera liar di komplek masjid, kera-kera itu tidak takut dengan pengunjung yang datang bahkan kera-kera tersebut berkumpul dengan aktivitas pengunjung yang ada disana.',
    author: 'Aedy',
    category: 'P5',
    image: "https://duniamasjid.islamic-center.or.id/wp-content/uploads/2012/03/masjid-saka-tunggal11.jpg",
    price: 0
  ),

  Places(
    id: 'A7',
    name: 'Gua Maria Kaliori',
    description: 'Kabupaten Banyumas juga memiliki tempat ziarah bagi umat Katolik,  yaitu Gua Maria Kaliori yang berada di Kecamatan Kalibagor. Tempat ini selalu ramai dikunjungi oleh umat Katolik pada bulan Desember untuk berziarah. Dari Kota Purwokerto, Gua Maria Kaliori ini berjarak 20 kilometer. Meskipun dikenal sebagai tempat peziarah umat Katolik, gua ini juga dibuka untuk umum sebagai wisata karena dikenal sebagai gua ziarah umat Katolik paling lengkap. Gua Maria Kaliori ini memiliki beberapa fasilitas pendukung, seperti Aula St. Yoseph, Pemakaman Uskup dan Imam, Taman Rosario Hudup, Gereja, toko suvenir, jalan salib, kapel ratu surga, pendapa bagi peziarah, ruang pengakuan dosa serta Rumah Retret Maria Immaculata yang mampu menampung hingga 150 orang.',
    author: 'Yesaya WisnuDanang Nur Ihsan',
    category: 'P5',
    image: "https://cdn-2.tstatic.net/jateng/foto/bank/images/tempat-ziarah-goa-maria.jpg",
    price: 0
  ),

  Places(
    id: 'A8',
    name: 'Taman Miniatur Dunia (Small World)',
    description: 'Salah satu taman yang harus di kunjungi anda ketika anda datang atau ingin mengajak rombongan anak anak anda datang ke Purwokerto adalah Taman Miniatur Dunia atau yang sering juga di sebut dengan nama Small World. Ketika anda berada di taman ini, anda akan merasakan tempat yang sangat indah dengan banyak hal yang bisa di pelajari. Jika biasanya kita hanya belajar dari gambar dan juga membaca buku, maka di tempat ini anda bisa melihat banyak miniatur miniatur dari negara negara di dunia, seperti lambang dari Singapura, ada juga miniatur dari Petronas Tower di Malaysia, tidak hanya itu, anda bisa menemukan beberapa ikon terkenal lainnya seperti Menara Pisa di Italia, dan juga White House di Amerika.Jika ingin datang ke tempat ini anda bisa langsung ke Jl. Raya Baturraden Barat No 270, Purwokerto. Jam operasional dari Taman miniatur dunia ini ada Setiap hari. Untuk jam operasionalnya sendiri Minggu hingga Kamis ada di pukul 07.00 pagi hingga 10.00 malam. Pada weekend Jumat dan Sabtu, anda bisa mengunjungi taman ini pada pukul 07.00 pagi hingga pukul 12 malam.',
    author: 'Laurentsia',
    category: 'P6',
    image: "https://cdn.idntimes.com/content-images/post/20190613/larasaticitraa-18255a9d5f063fb2ac09131e6d379a11.jpg",
    price: 15000
  ),
  
  Places(
    id: 'A9',
    name: 'Museum BRI (Bank Rakyat Indonesia)',
    description: 'Salah satu tempat selanjutnya yang bisa anda kunjungi menjadi tempat edukasi di Purwokerto adalah Museum BRI ini. Museum yang satu ini termasuk museum unik di Indonesia dengan ada banyak sejarah yang perlu anda ketahui mengenai bank BRI dari awal mula berdirinya hingga sekarang ini. Purwokerto merupakan kota awal dimana Museum ini di bangun, hingga hari ini. Banyak barang barang yang langka dan yang di gunakan pada awal berdirinya BRI ini sendiri, dari mulai telepon, hingga koleksi mata uang Indonesia pada masa lalu dan juga uang dari luar negeri. Museum ini merupakan salah daru wisata edukasi yang perlu anda kunjungi jika anda berada di Purwokerto. Anda bisa menemukan museum ini di Jl. Jendral Sudirman dan hanya di buka pada hari Minggu hingga hari Kamis. Jam kerjanya pun hanya mulai pukul 08.00 pagi hingga 15.00 sore. Jika anda ke tempat ini, anda tidak akan di pungut biaya untuk masuknya, jadi anda dapat belajar tanpa membayar juga dong.',
    author: 'Laurentsia',
    category: 'P6',
    image: "https://assets.pikiran-rakyat.com/crop/0x0:0x0/x/photo/2021/11/08/4798784.jpeg",
    price: 0
  ),

  Places(
    id: 'A10',
    name: 'Desa Wisata Ketenger',
    description: 'Tempat selanjutnya yang bisa anda datangi adalah Desa Wisata Ketenger. Salah satu desa yang ada di lereng Gunung Slamet ini membuat tempat ini sangat indah dan juga alami, asri. Salah satu desa ini lah yang menjadi desa wisata. Anda bisa melakukan banyak kegiatan di tempat ini, anda bisa memulai pendakian gunung, dan juga tentunya anda bisa beredukasi mengenai kehidupan pedesaan yang tradisional. Selain itu di tempat ini juga ada banyak acara seperti penjelajahan hutan, dan banyak hal lainnya. Anda dapat belajar dan beredukasi di desa yang terbuka untuk umum ini sendiri. Jika anda ingin mencari tempat ini, anda bisa datang ke Ketenger, Baturraden sendiri. Desa ini akan di buka untuk wisatawan yang mau datang Setiap hari, namun ada waktu yang perlu di taati yaitu pukul 07.00 pagi hari hingga pukul 18.00 sore hari. Anda tidak perlu membayar tiket masuk jika ingin berada di tempat ini.',
    author: 'Angie',
    category: 'P6',
    image: "https://www.pinhome.id/info-area/wp-content/uploads/2022/03/Dede-Anto-Sapnudin.jpg",
    price: 0
  ),

  Places(
    id: 'A11',
    name: 'Bakso pekih',
    description: 'Bakso pekih sudah ada sejak 20 tahun yang lalu dan sampai saat ini, bakso pekih masih menjadi primadona warga ngapak Purwokerto. Bakso ini tipikal bakso dengan kuah bening nan gurih. Kamu dapat membeli satu porsi bakso pekih dengan merogoh kocek sekitar Rp15.000. Bakso Pekih berlokasi di Jl. Pekih, Purwokerto, Sokanegara, Kec. Purwokerto Tim., Kabupaten Banyumas, Jawa Tengah.',
    author: 'Destria Indria',
    category: 'P4',
    image: "https://media-cdn.yummyadvisor.com/store/2c863857-d937-495e-847e-cbaad98b4bce.jpg?x-oss-process=style/type_11",
    price: 15000
  ),
  
  Places(
    id: 'A12',
    name: 'Rumah Makan Lombok Idjo',
    description: 'Mengusung konsep resto keluarga, Rumah Makan Lombok Idjo cocok bagi kalian yang ingin menghabiskan akhir pekan dengan makan bersama keluarga. Berlokasi di Jl Mangunjaya, Purwokerto Lor, Kec. Purwokerto Tim., Kabupaten Banyumas, Jawa Tengah.',
    author: 'Destria Indria',
    category: 'P4',
    image: "https://i0.wp.com/banyumas.org/wp-content/uploads/2022/05/soto-sami-asih-tempat-makan-di-purwokerto-yang-bagus-1.jpeg?ssl=1",
    price: 18000
  ),  

  Places(
    id: 'A13',
    name: 'Bakso dan Soto Sami Asih',
    description: 'Bisa dibilang Bakso dan Soto Sami Asih adalah saingannya bakso pekih. Mengapa demikian? Karena warung bakso yang satu ini tidak kalah ramai dari bakso pekih. Menyediakan menu bakso dan soto dengan tipikal kuah keruh yang sangat kaya akan rasa. Harga yang ditawarkan sekitar Rp15.000 per mangkuk. Bakso dan Soto Sami Asih berlokasi di Jl. Pramuka No.23, Sodagaran, Purwokerto Kulon, Kec. Purwokerto Sel., Kabupaten Banyumas, Jawa Tengah.',
    author: 'Destria Indria',
    category: 'P4',
    image: "https://img.restaurantguru.com/rbf1-Lombok-Idjo-Purwokerto-exterior.jpg",
    price: 15000
  ),  

  Places(
    id: 'A14',
    name: 'Rumah Lengger',
    description: 'Berisikan berbagai barang peninggalan penari lengger serta bukti sejarah tari lengger diBanyumas, lengger merupakan tarian yang ditarikan oleh laki-laki. Begitu Unik dan khas bukan? dalam rumah lengger tersebut wisatawan juga bisa melihat alat musik yang digunakan untuk mengiringi tarian lengger. Berbagai keunikan dan cerita tari lengger yang menarik untuk disimak, selain itu berbagai lukisan yang ada menyuguhkan keindahan yang akan memanjakan mata para pengunjung.',
    author: 'Lausensia',
    category: 'P3',
    image: "https://blue.kumparan.com/image/upload/fl_progressive,fl_lossy,c_fill,q_auto:best,w_640/v1634025439/0180ec1e83929ef45c7d834151277969.jpg",
    price: 10000
  ),

  Places(
    id: 'A15',
    name: 'Sumur mas',
    description: 'Sumur Mas yang penuh cerita dan sejarah, Sumur ini dikaitkan dengan sejarah lahirnya nama "Banyumas" selain itu banyak cerita dan mitos yang meliputi sumur ini. Para pengunjung akan dibantu oleh pemandu dan diceritakan tentang sejarah dari sumur Mas selain itu posisi sumur yang dekat dengan taman juga bisa membantu pengujung saat ingin beristirahat sembari melihat-lihat sumur yang penuh sejarah dan misteri ini',
    author: 'Lausensia',
    category: 'P3',
    image: "https://cdn2.tstatic.net/jateng/foto/bank/images/sumur-tua_20180122_191349.jpg",
    price: 0
  ),  

  Places(
    id: 'A16',
    name: 'Museum Wayang',
    description: 'Beralih ke Museum Wayang Banyumas dalam museum para pengunjung akan melihat berbagai alat kesenian berupa gamelan, dan wayang yang dilengkapi dengan cerita sejarah serta kereta kencana. Dalam museum ini para pengunjung bisa belajar mengenai kesenian dan gamelan, untuk tiket masuk ke museum ini sangat terjangkau tetapi kita sudah bisa mendapat banyak ilmu dari museum ini.',
    author: 'Lausensia',
    category: 'P3',
    image: "https://img.restaurantguru.com/rbf1-Lombok-Idjo-Purwokerto-exterior.jpg",
    price: 1000
  ),
];